﻿from params import *
from AudioPacket import *
from Buffer import *
from Sender import *
from Receiver import *

if __name__ == '__main__':
    pkt = AudioPacket("he salut hein !")
    obj_sent = Sender().transmit(pkt, 19e3)
    # ICI PLAY buffer.data

    # ICI LISTEN and get the buffer, unpack and deliver the original object
    samples = Buffer(obj_sent).unpack()
    obj_received = Receiver().deliver(samples, 19e3)

    print "Obj received: ", obj_received.content

    if pkt.content.strip() == obj_received.content.strip():
        print "Message received correctly"
    else:
        print "Error when listening to channel"
