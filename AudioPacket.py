﻿import binascii
import cPickle as pickle

class AudioPacket:
    def __init__(self, content):
        self.content = content

    def __str__(self):
        return (self.content)

    def serialize(self):
        pkt = AudioPacket(self.content)
        obj_serialized = pickle.dumps(pkt,2)
        obj_to_bin = bin(int(binascii.hexlify(obj_serialized), 16))
        obj_to_array = list(obj_to_bin)
        return obj_to_array

    def unserialize(self):
        bin_obj_unserialized = binascii.unhexlify('%x' % int(self.content, 2))
        obj_unserialized = pickle.loads(bin_obj_unserialized)
        return obj_unserialized