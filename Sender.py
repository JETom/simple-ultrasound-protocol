﻿import numpy
import signals
from params import *
from library import *
from Buffer import *

class Sender:
    def __init__(self, sr=SAMPLE_RATE, bps=BITS_PER_SECOND, lbw=LIM_BANDWIDTH):
        self.sample_rate = sr
        self.bits_per_second = bps
        self.lim_bandwidth = lbw

    # Convert message bits into samples, then use samples
    # to modulate the amplitude of sinusoidal carrier.
    # Limit the bandwidth of the transmitted signal to
    # bw Hz.  
    def am_transmit(self, bits,samples_per_bit,sample_rate,fc,bw):
        # use helper function to create a sampled_waveform by
        # converting each bit into samples_per_bit samples
        samples = signals.symbols_to_samples(bits,samples_per_bit, sample_rate)
        bandlimited_samples = samples.filter('low-pass', cutoff=bw)
        # now multiply by sine wave of specified frequency
        return bandlimited_samples.modulate(hz=fc)

    def transmit(self, packet, freq):
        # may need to pad so it is exactly the length expected for the sequence
        max_len_msg = (LEN_BIN_SEQUENCE-(SIZE_AUDIO_PACKET_EMPTY+16))/8
        len_content = len(packet.content)
        if (len_content!= max_len_msg):
            if (len_content < max_len_msg) :
                packet.content = packet.content.ljust(max_len_msg)
            else :
                packet.content = packet.content[0:max_len_msg]

        # serialize packet
        pkt_serialized = packet.serialize()
        bin_sequence = numpy.zeros(len(pkt_serialized),dtype=numpy.int)
        bin_sequence[1:-1] = pkt_serialized[2:]

        # send it through transmitter, modulate to legal freq
        spb = samples_per_bit(self.sample_rate, self.bits_per_second)
        fc = quantize_frequency(freq, self.sample_rate, LEN_BIN_SEQUENCE*spb)
        rf = self.am_transmit(bin_sequence,spb,self.sample_rate, fc, self.lim_bandwidth)

        return (Buffer(rf.samples).pack())