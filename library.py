﻿########### LIBRARY ##########
# ensure spectral plots make sense
def quantize_frequency(f,fs,N):
    #assert N&1,"N must be odd!"
    f1 = float(fs)/float(N)
    return int(f/f1)*f1

def samples_per_bit(sample_rate,bits_per_second):
    bps = int(sample_rate/bits_per_second)
    if (bps & 1) == 0: bps += 1
    return bps