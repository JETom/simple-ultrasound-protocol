﻿import numpy
import signals
from params import *
from library import *
from AudioPacket import *

class Receiver:
    def __init__(self, sr=SAMPLE_RATE, bps=BITS_PER_SECOND, lbw=LIM_BANDWIDTH, length_seq=LEN_BIN_SEQUENCE):
        self.sample_rate = sr
        self.bits_per_second = bps
        self.lim_bandwidth = lbw
        self.len_bin_sequence = length_seq

    # receive amplitude-modulated transmission
    def am_receive(self, samples,fc,samplesperbit,channel_bw):
        demodulated_samples = samples.modulate(hz=fc)
        filtered_samples = demodulated_samples.filter('low-pass', cutoff=channel_bw)
        centers = signals.samples_to_centers(filtered_samples, samples_per_symbol=samplesperbit)
        bit_sample_array = filtered_samples[centers]
        threshold = numpy.average(bit_sample_array)
        return (numpy.array(bit_sample_array)>threshold)*1

    def deliver(self, samples, freq):
        spb = samples_per_bit(self.sample_rate, self.bits_per_second)
        nsamples = self.len_bin_sequence*spb
        fc = quantize_frequency(freq,self.sample_rate, nsamples)
        rc = signals.sinusoid(nsamples, fc, self.sample_rate, 1.0, 0.0)
        rc.samples = samples

        received = self.am_receive(rc,fc,spb,self.lim_bandwidth)
        received = ''.join(str(x) for x in received)
        received = (received[:1] + '0b' + received[1:-1])[1:]
        pkt = AudioPacket(received).unserialize()
        pkt.content = pkt.content.strip()

        return pkt