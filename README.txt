The purpose of this project is to create an application that can allow machines to communicate with one another without using Wifi or Bluetooth radios. Hence, the communication will be done by using microphones and speakers usually embedded in our devices (e.g. computers) at frequencies inaudible to human ears.

The underlying goal of the project is to present an alternative mean of communication, always operational, which makes use of hardware not designed for wireless transmission, in situation where no conventional networks (e.g. Wifi networks) are available.

In order to make it work, a protocol has to be designed before starting the development of an application. Indeed, the process of recording and monitoring sounds at high frequencies may be subject to physical constraints such as sensitivity of the microphone, environmental noise, etc. Thus, these constraints may lead to an effect similar to packet loss.

The protocol will be designed to enable packets exchange between multiple hosts. This means any kind of data could be virtually transmitted, depending on the application that implements the protocol. The objective of the program developed with this protocol is to enable simple instant-message service (like IRC).

Our protocol and application will be developed in Python using a PortAudio-based library to deal with audio signals. The communication program should be usable on any computer platform (Windows, Linux and MacOSX).