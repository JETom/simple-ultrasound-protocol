﻿#!/usr/bin/env python
import numpy,wave,os,sys,time
import matplotlib.pyplot as p

################################################################################
## helper functions
################################################################################

# copy over some defns from numpy and matplotlib as a convenience...
ion = p.ion
pi = numpy.pi

###################### PLAY (can be removed, only for test) ########################
# set up for audio playback
audio = None
try:
    import ossaudiodev
    audio = 'oss'
except ImportError:
    try:
        import pyaudio
        audio = 'pyaudio'
    except ImportError:
        pass

def play(samples,sample_rate,gain=None):
    nsamples = len(samples)
    
    # compute appropriate gain if none supplied
    if gain is None:
        gain = 1.0/max(numpy.absolute(samples))

    # convert -1V to 1V waveform to signed 16-bit ints
    asamples = numpy.zeros(nsamples,dtype=numpy.int16)
    numpy.multiply(samples,32767.0 * gain,asamples.astype(samples.dtype))
    offset = 0   # where we are in sending data
    nremaining = nsamples

    # send audio samples to output device
    if audio == 'oss':
        stream = ossaudiodev.open('w')
        fmt,chans,rate = stream.setparameters(ossaudiodev.AFMT_S16_LE,1,int(sample_rate))
        assert rate==sample_rate,\
               "%g is not a supported sample rate for audio playback" % sample_rate

        while nremaining > 0:
            can_send = min(stream.obuffree(),nremaining)
            if can_send > 0:
                end = offset + can_send
                stream.write(asamples[offset:end].tostring())
                offset = end
                nremaining -= can_send
                    
        stream.close()

    elif audio == 'pyaudio':
        dev = pyaudio.PyAudio()
        devinfo = dev.get_default_output_device_info()
        assert dev.is_format_supported(sample_rate,
                                       output_device = devinfo['index'],
                                       output_channels = 1,
                                       output_format = pyaudio.paInt16),\
               "%g is not a supported sample rate for audio playback" % sample_rate
        stream = dev.open(int(sample_rate),1,pyaudio.paInt16,output=True)

        while nremaining > 0:
            can_send = min(stream.get_write_available(),nremaining)
            if can_send > 0:
                end = offset + can_send
                stream.write(asamples[offset:end].tostring(),num_frames=can_send)
                offset = end
                nremaining -= can_send

        stream.close()

    else:
        assert False,"Sorry, no audio device library could be found"
############################### END PLAY ################################

############################### TAPS ####################################

# compute number of taps given sample_rate and transition_width.
# Stolen from the gnuradio firdes routines
def compute_ntaps(transition_width,sample_rate,window):
    delta_f = float(transition_width)/sample_rate
    width_factor = {
        'hamming': 3.3,
        'hann': 3.1,
        'blackman': 5.5,
        'rectangular': 2.0,
        }.get(window,None)
    assert width_factor,\
           "compute_ntaps: unrecognized window type %s" % window
    ntaps = int(width_factor/delta_f + 0.5)
    return (ntaps & ~0x1) + 1   # ensure it's odd

# compute specified window given number of taps
# formulae from Wikipedia
def compute_window(window,ntaps):
    order = float(ntaps - 1)
    if window == 'hamming':
        return [0.53836 - 0.46164*numpy.cos((2*numpy.pi*i)/order)
                for i in xrange(ntaps)]
    elif window == 'hann' or window == 'hanning':
        return [0.5 - 0.5*numpy.cos((2*numpy.pi*i)/order)
                for i in xrange(ntaps)]
    elif window == 'bartlett':
        return [1.0 - abs(2*i/order - 1)
                for i in xrange(ntaps)]
    elif window == 'blackman':
        alpha = .16
        return [(1-alpha)/2 - 0.50*numpy.cos((2*numpy.pi*i)/order)
                - (alpha/2)*numpy.cos((4*numpy.pi*i)/order)
                for i in xrange(ntaps)]
    elif window == 'nuttall':
        return [0.355768 - 0.487396*numpy.cos(2*numpy.pi*i/order)
                         + 0.144232*numpy.cos(4*numpy.pi*i/order)
                         - 0.012604*numpy.cos(6*numpy.py*i/order)
                for i in xrange(ntaps)]
    elif window == 'blackman-harris':
        return [0.35875 - 0.48829*numpy.cos(2*numpy.pi*i/order)
                        + 0.14128*numpy.cos(4*numpy.pi*i/order)
                        - 0.01168*numpy.cos(6*numpy.pi*i/order)
                for i in xrange(ntaps)]
    elif window == 'blackman-nuttall':
        return [0.3635819 - 0.4891775*numpy.cos(2*numpy.pi*i/order)
                          + 0.1365995*numpy.cos(4*numpy.pi*i/order)
                          - 0.0106411*numpy.cos(6*numpy.py*i/order)
                for i in xrange(ntaps)]
    elif window == 'flat top':
        return [1 - 1.93*numpy.cos(2*numpy.pi*i/order)
                  + 1.29*numpy.cos(4*numpy.pi*i/order)
                  - 0.388*numpy.cos(6*numpy.py*i/order)
                  + 0.032*numpy.cos(8*numpy.py*i/order)
                for i in xrange(ntaps)]
    elif window == 'rectangular' or window == 'dirichlet':
        return [1 for i in xrange(ntaps)]
    else:
        assert False,"compute_window: unrecognized window type %s" % window

# Stolen from the gnuradio firdes routines
def fir_taps(type,cutoff,sample_rate,
                 window='hamming',transition_width=None,ntaps=None,gain=1.0):
    if ntaps:
        ntaps = (ntaps & ~0x1) + 1   # make it odd
    else:
        assert transition_width,"compute_taps: one of ntaps and transition_width must be specified"
        ntaps = compute_ntaps(transition_width,sample_rate,window)

    window = compute_window(window,ntaps)

    middle = (ntaps - 1)/2
    taps = [0] * ntaps
    fmax = 0

    if isinstance(cutoff,tuple):
        fc = [float(cutoff[i])/sample_rate for i in (0,1)]
        wc = [2*numpy.pi*fc[i] for i in (0,1)]
    else:
        fc = float(cutoff)/sample_rate
        wc = 2*numpy.pi*fc

    if type == 'low-pass':
        # for low pass, gain @ DC = 1.0
        for i in xrange(ntaps):
            if i == middle:
                coeff = (wc/numpy.pi) * window[i]
                fmax += coeff
            else:
                n = i - middle
                coeff = (numpy.sin(n*wc)/(n*numpy.pi)) * window[i]
                fmax += coeff
            taps[i] = coeff
    elif type == 'high-pass':
        # for high pass gain @ nyquist freq = 1.0
        for i in xrange(ntaps):
            if i == middle:
                coeff = (1.0 - wc/numpy.pi) * window[i]
                fmax += coeff
            else:
                n = i - middle
                coeff = (-numpy.sin(n*wc)/(n*numpy.pi)) * window[i]
                fmax += coeff * numpy.cos(n*numpy.pi)
            taps[i] = coeff
    elif type == 'band-pass':
        # for band pass gain @ (fc_lo + fc_hi)/2 = 1.0
        # a band pass filter is simply the combination of
        #   a high-pass filter at fc_lo  in series with
        #   a low-pass filter at fc_hi
        # so convolve taps to get the effect of composition in series
        for i in xrange(ntaps):
            if i == middle:
                coeff = ((wc[1] - wc[0])/numpy.pi) * window[i]
                fmax += coeff
            else:
                n = i - middle
                coeff = ((numpy.sin(n*wc[1]) - numpy.sin(n*wc[0]))/(n*numpy.pi)) * window[i]
                fmax += coeff * numpy.cos(n*(wc[0] + wc[1])*0.5)
            taps[i] = coeff
    elif type == 'band-reject':
        # for band reject gain @ DC = 1.0
        # a band reject filter is simply the combination of
        #   a low-pass filter at fc_lo   in series with a
        #   a high-pass filter at fc_hi
        # so convolve taps to get the effect of composition in series
        for i in xrange(ntaps):
            if i == middle:
                coeff = (1.0 - ((wc[1] - wc[0])/numpy.pi)) * window[i]
                fmax += coeff
            else:
                n = i - middle
                coeff = ((numpy.sin(n*wc[0]) - numpy.sin(n*wc[1]))/(n*numpy.pi)) * window[i]
                fmax += coeff
            taps[i] = coeff
    else:
        assert False,"compute_taps: unrecognized filter type %s" % type

    gain = gain / fmax
    for i in xrange(ntaps): taps[i] *= gain
    return taps
############################### END TAPS ####################################

################################################################################
## sampled_waveform base class
################################################################################

class sampled_waveform:
    def __init__(self,samples,sample_rate,domain='time'):
        if not isinstance(samples,numpy.ndarray):
            samples = numpy.array(samples,dtype=numpy.float,copy=True)
        self.samples = numpy.array(samples, copy=True)   # a numpy array
        self.nsamples = len(samples)
        self.sample_rate = sample_rate
        self.domain = domain

    def __len__(self):
        return len(self.samples)

    def __getitem__(self,key):
        return self.samples.__getitem__(key)

    def __setitem__(self,key,value):
        if isinstance(value,sampled_waveform):
            value = value.samples
        self.samples.__setitem__(key,value)

    def __str__(self):
        return str(self.samples)+('@%d samples/sec' % self.sample_rate)

    def convolve(self,taps):
        conv_res = numpy.convolve(self.samples,taps)
        offset = len(taps)/2
        return sampled_waveform(conv_res[offset:offset+self.nsamples],
                                sample_rate=self.sample_rate,
                                domain=self.domain)

    def modulate(self,hz,phase=0.0,gain=1.0):

        periods = float(self.nsamples*hz)/float(self.sample_rate)
        if abs(periods - round(periods)) > 1.0e-6:
            print "Warning: Non-integral number of modulation periods"
            print "nsamples=%d hz=%f sample_rate=%d periods=%f" % (self.nsamples, hz,self.sample_rate,periods)

        result = sinusoid(hz=hz,nsamples=self.nsamples,
                          sample_rate=self.sample_rate,phase=phase,
                          amplitude=gain)
        numpy.multiply(self.samples,result.samples,result.samples)
        return result

    def filter(self,type,cutoff,
               window='hamming',transition_width=None,ntaps=None,error=0.05,gain=1.0):
        if ntaps is None and transition_width is None:
            # ensure sufficient taps to represent a frequency resolution of error*cutoff
            ntaps = int(float(self.sample_rate)/(float(cutoff)*error))
            if ntaps & 1: ntaps += 1
        taps = fir_taps(type,cutoff,self.sample_rate,
                        window=window,transition_width=transition_width,
                        ntaps=ntaps,gain=gain)
        return self.convolve(taps)

################################################################################
## sources
################################################################################

class sinusoid(sampled_waveform):
    def __init__(self,nsamples,hz,sample_rate,amplitude,phase):
        assert hz <= sample_rate/2,"hz cannot exceed %gHz" % (sample_rate/2)
        phase_step = (2*numpy.pi*hz)/sample_rate
        temp = numpy.arange(nsamples,dtype=numpy.float64) * phase_step + phase
        numpy.cos(temp,temp)
        numpy.multiply(temp,amplitude,temp)
        sampled_waveform.__init__(self,temp,sample_rate=sample_rate)


################################################################################
## operations
################################################################################

def symbols_to_samples(symbols,samples_per_symbol,sample_rate,sample_values=None):
    nsymbols = len(symbols)
    if sample_values is None:
        sample_values = numpy.arange(nsymbols)
    samples = numpy.zeros(nsymbols*samples_per_symbol,dtype=numpy.int)
    for j in xrange(samples_per_symbol):
        samples[j::samples_per_symbol] = sample_values[symbols]
    return sampled_waveform(samples,sample_rate=sample_rate)

def samples_to_centers(samples,samples_per_symbol):
    nsamples = len(samples)

    # try to find a good threshold for digitizing the samples
    threshold = numpy.average(samples.samples)

    # digitize the samples
    dsamples = numpy.zeros(nsamples,dtype=numpy.int)
    dsamples[samples.samples >= threshold] = 1

    # now use a little control loop to figure out where to sample
    centers = numpy.zeros(nsamples/samples_per_symbol,dtype=numpy.int)
    sample_point = samples_per_symbol/2
    sample_end = samples_per_symbol - 1
    index = 0
    count = 0
    last = 0
    for i in xrange(nsamples):
        current = dsamples[i]
        if count == sample_end or last != current:
            count = 0
            last = current
        else:
            if count == sample_point and index < centers.size:
                centers[index] = i
                index += 1
            count = count + 1

    centers.resize(index)
    return centers