﻿import struct

class Buffer:
    def __init__(self, data):
        self.data = data

    def pack(self):
        self.data = ''.join([struct.pack('f', frame) for frame in self.data])
        return self.data

    def chunks(self, l, n):
        for i in xrange(0, len(l), n):
            yield l[i:i+n]

    def unpack(self):
        self.data = [struct.unpack('f', frame)[0] for frame in list(self.chunks(self.data, 4))]
        return self.data